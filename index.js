import express from "express";
import dotenv from "dotenv";
import cocktails from "./server/cocktails/cocktails.routes.js";

const APP = express();
const PORT = process.env.PORT ?? 3000;
dotenv.config();

APP.use(express.json());
APP.use(express.urlencoded({ extended: true }));
APP.get("/hello", (req, res) => res.send("hello"));

/**
 * Route initialization
 */
APP.use("/cocktails", cocktails);

APP.listen(PORT, async () => {
  console.log("\x1b[43m%s\x1b[0m", `API listening on ${PORT}`);
});
