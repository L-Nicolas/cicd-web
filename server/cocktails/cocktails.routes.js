import { Router } from "express";
import CocktailsCtrl from "./cocktails.controller.js";

const router = Router();
router.post("/create", CocktailsCtrl.create); //Ajouter d'un cocktail
router.post("/update", CocktailsCtrl.update); //Modification d'un cocktail
router.get("/read", CocktailsCtrl.update); //Récupération d'un cocktail

export default router;
