import express from "express";
import CocktailsMdl from "./cocktails.model.js";

const { Request, Response } = express;
export default class CocktailsCtrl {
  static fnc = async (req = Request, res = Response) => {
    res.status(200).send("create");
  };

  static create = (req = Request, res = Response) => {
    let code = 400;
    let error = true;
    let response = { error: error, message: "Bad request", data: [] };

    if (Object.keys(req.body).length > 0) {
      const dataIpt = [
        { label: "id", type: "Number" },
        { label: "nom", type: "string" },
        { label: "price", type: "string" },
        { label: "alcool", type: "string" },
        { label: "ingredient", type: "concatString" },
      ];
      let listError = this.verifSecure(dataIpt, req.body);

      if (listError.length > 0) {
        response.message = "Erreur";
        response.data.push(listError);
      } else {
        try {
          let message = "Impossible de créer le cocktail";
          response = { error: error, message: message, data: [data] };
          res.status(200).send(response);
        } catch (error) {
          console.log(error);
          res.status(400).send(response);
        }
        res.status(code).send(response);
      }
      res.status(code).send(response);
    }
    res.status(code).send(response);
  };

  static update = (req = Request, res = Response) => {
    let code = 400;
    let error = true;
    let response = { error: error, message: "Bad request", data: [] };

    if (Object.keys(req.body).length > 0) {
      const dataIpt = [
        { label: "id", type: "Number" },
        { label: "nom", type: "string" },
        { label: "price", type: "string" },
        { label: "alcool", type: "string" },
        { label: "ingredient", type: "concatString" },
      ];
      let listError = this.verifSecure(dataIpt, req.body);

      if (listError.length > 0) {
        response.message = "Erreur";
        response.data.push(listError);
      } else {
        try {
          let message = "Impossible de modifier le cocktail";
          response = { error: error, message: message, data: [data] };
          res.status(200).send(response);
        } catch (error) {
          console.log(error);
          res.status(400).send(response);
        }
        res.status(code).send(response);
      }
      res.status(code).send(response);
    }
    res.status(code).send(response);
  };

  static read = (req = Request, res = Response) => {
    let code = 400;
    let error = true;
    let response = { error: error, message: "Bad request", data: [] };

    if (Object.keys(req.body).length > 0) {
      const dataIpt = [
        { label: "id", type: "Number" },
        { label: "nom", type: "string" },
        { label: "price", type: "string" },
        { label: "alcool", type: "string" },
        { label: "ingredient", type: "concatString" },
      ];
      let listError = this.verifSecure(dataIpt, req.body);

      if (listError.length > 0) {
        response.message = "Erreur";
        response.data.push(listError);
      } else {
        try {
          let message = "Cocktail inconnu";
          response = { error: error, message: message, data: [data] };
          res.status(200).send(response);
        } catch (error) {
          console.log(error);
          res.status(400).send(response);
        }
        res.status(code).send(response);
      }
      res.status(code).send(response);
    }
    res.status(code).send(response);
  };

  static verifSecure = (schemaData, dataToVerif) => {
    let listError = [];
    schemaData.forEach((val) => {
      if (!Object.keys(dataToVerif).includes(val.label)) {
        listError.push(`Champ ${val.label} manquant`);
      }
    });

    Object.entries(dataToVerif).forEach(([key, value]) => {
      if (schemaData.find((item) => item.label.match(key))) {
        let typeVal = toString(schemaData.find((item) => item.label.match(key))?.type);
        if (!value || value === "") {
          listError.push(`Champ ${key} vide`);
        } else {
          if (typeof value === typeVal) {
            if (typeVal === "string") {
              let reg = new RegExp("[^a-z0-9-àáâãäåòóôõöøèéêëçìíîïùúûüÿñ_s@.]+", "gmi");
              let testReg = reg.test(value);
              if (testReg) {
                console.log(typeVal);
                listError.push(`Format du champ ${key} incorrecte`);
              }
            } else if (typeVal === "concatString") {
              let reg = new RegExp("[^a-z0-9-àáâãäåòóôõöøèéêëçìíîïùúûüÿñ_s@.|]+", "gmi");
              let testReg = reg.test(value);
              if (testReg) {
                console.log(typeVal);
                listError.push(`Format du champ ${key} incorrecte`);
              }
            }
          } else {
            listError.push(`Format du champ ${key} incorrecte`);
          }
        }
      } else if (schemaData.find((item) => item.label.match(key)) === undefined) {
        listError.push(`Champ ${key} non autorisé`);
      }
    });
    console.log(listError);

    return listError;
  };
}
